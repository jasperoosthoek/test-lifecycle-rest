import logo from './logo.svg';
import './App.css';

import { RestApi, createAgent } from '@mkrause/lifecycle-rest';
import { makeStorable } from '@mkrause/lifecycle-rest/lib-cjs/loader/StorablePromise.js';
import * as t from 'io-ts';

// Define your data types
const User = t.type({ name: t.string });
const UsersCollection = t.array(User);

// Create an HTTP agent (axios)
const agent = createAgent({
    baseURL: 'https://example.com/api',
});

const applyStorable = (context, method, args) => {
  const resourceConfig = RestApi.getResourceConfig(context);
  
  return makeStorable(method.call(context, resourceConfig, ...args), {
    location: resourceConfig.store,
    operation: 'put',
  });
};

const api = RestApi({ agent }, RestApi.Item(t.unknown,  {
    resources: {
        users: RestApi.Collection(UsersCollection, {
            uri: 'users',
            
            // Custom methods
            methods: {
              async searchAsync({ agent, uri }, params) {
                return await agent.get(uri);
              },
              search(...args) {
                return applyStorable(this, this.searchAsync, args);
              },
            },
  
            entry: RestApi.Item(User),
        }),
    },
}));
const test = async () => {
  // Call the API directly
  const users = await api.users.list(); // GET /api/users

  // Index into a collection to access an entry (configurable through the `entry` property)
  const john = await api.users('john').get(); // GET /api/users/john
}

const App = () => {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
